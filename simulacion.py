from random import randint
from mesa import Agent, Model
from mesa.space import MultiGrid
from mesa.time import RandomActivation
from mesa.visualization.modules import CanvasGrid, TextElement
from mesa.visualization.ModularVisualization import ModularServer

import numpy as np


class TotalSteps(TextElement):
    """
    Display a text count of how many happy agents = 0 xd.
    """

    def __init__(self):
        pass

    def render(self, model):
        return "Pasos en total: "


class Car(Agent):
    def __init__(self, model, pos, route, speed, acc, ct, carL, trafficL):
        super().__init__(model.next_id(), model)
        self.pos = pos
        self.route = route
        self.speed = speed
        self.max_speed = 5
        self.acceleration = acc
        self.direction = 0
        self.car_type = ct
        self.carList = carL
        self.trafficList = trafficL
        self.active = True
    
    def step(self):
        if ( self.route != [] and self.pos == (self.route[0][0], self.route[0][1])):
                self.route.pop(0)
        if self.route != []:
            self.calculateDirection()
            print(str(self.unique_id) + " dir:  " + str(self.direction))
            if(self.direction == 0):
                self.speed[0] = self.checkAhead()

                next_move = (self.pos[0] + self.speed[0], self.pos[1])

                if(next_move[0] < self.route[0][0] and (next_move[0] < self.model.grid.width)):
                    self.model.grid.move_agent(self, next_move)
                else:
                    self.model.grid.move_agent(self, (self.route[0][0], self.route[0][1]))

            if(self.direction == 1):
                self.speed[0] = self.checkAhead()

                next_move = (self.pos[0] + self.speed[0], self.pos[1])

                if(next_move[0] >= self.route[0][0] and (next_move[0] >= 0)):
                    self.model.grid.move_agent(self, next_move)
                else:
                    self.model.grid.move_agent(self, (self.route[0][0], self.route[0][1]))

            if(self.direction == 2):
                self.speed[1] = self.checkAhead()

                next_move = (self.pos[0], self.pos[1] + self.speed[1])

                if(next_move[1] >= self.route[0][1] and (next_move[1] >= 0)):
                    self.model.grid.move_agent(self, next_move)
                else:
                    self.model.grid.move_agent(self, (self.route[0][0], self.route[0][1]))

            if(self.direction == 3):
                self.speed[1] = self.checkAhead()

                next_move = (self.pos[0], self.pos[1] + self.speed[1])

                if(next_move[1] < self.route[0][1] and (next_move[1] < self.model.grid.height)):
                    self.model.grid.move_agent(self, next_move)
                else:
                    self.model.grid.move_agent(self, (self.route[0][0], self.route[0][1]))
        else:
            self.active = False

    def calculateDirection(self):
        if self.pos[0] < self.route[0][0]:
            self.direction = 0
            self.speed[0] = abs(self.speed[0])
            self.speed[1] = 0
        if self.pos[0] > self.route[0][0]:
            self.direction = 1
            self.speed[0] = abs(self.speed[0]) * -1
            self.speed[1] = 0
        if self.pos[1] > self.route[0][1]:
            self.direction = 2
            self.speed[0] = 0
            self.speed[1] = abs(self.speed[1]) * -1
        if self.pos[1] < self.route[0][1]:
            self.direction = 3
            self.speed[0] = 0
            self.speed[1] = abs(self.speed[1])

    def find_smol_value(self, val1, val2):
        if(val1 < val2):
            return val1
        return val2

    def find_big_value(self, val1, val2):
        if(val1 < val2):
            return val2
        return val1

    def checkAhead(self):
        min_val = 100
        max_val = -100
        for tlight in self.trafficList:
            if(not(tlight.state)):
                if(self.direction == 0 and tlight.direction == 0 and self.pos[0] <= tlight.pos[0]  and self.pos[1] == tlight.pos[1]):
                    if((tlight.pos[0] - self.pos[0]) <= 60):
                        min_val = self.find_smol_value(self.decelerate_on_x(), min_val)
                    if((tlight.pos[0] - self.pos[0]) <= 10):
                        min_val = self.find_smol_value(0, min_val)
                if(self.direction == 1 and tlight.direction == 1 and self.pos[0] >= tlight.pos[0]  and tlight.pos[1] == self.pos[1]):
                    if((self.pos[0] - tlight.pos[0]) <= 60):
                        max_val = self.find_big_value(self.decelerate_on_x(), max_val)
                    if((self.pos[0] - tlight.pos[0]) <= 10):
                        max_val = self.find_big_value(0, max_val)
                if(self.direction == 2 and tlight.direction == 2 and self.pos[1] >= tlight.pos[1] and tlight.pos[0] == self.pos[0]):
                    if((self.pos[1] - tlight.pos[1]) <= 60):
                        max_val = self.find_big_value(self.decelerate_on_y(), max_val)
                    if((self.pos[1] - tlight.pos[1]) <= 10):
                        max_val = self.find_big_value(0, max_val)
                if(self.direction == 3 and tlight.direction == 3 and self.pos[1] <= tlight.pos[1] and self.pos[0] == tlight.pos[0]):
                    if((tlight.pos[1] - self.pos[1]) <= 60):
                        min_val = self.find_smol_value(self.decelerate_on_y(), min_val)
                    if((tlight.pos[1] - self.pos[1]) <= 10):
                        min_val = self.find_smol_value(0, min_val)
        for car in self.carList:
            if(car.unique_id != self.unique_id and car.active):
                if(car.pos[1] == self.pos[1] and self.direction == 0 and car.direction == 0):
                    if(self.pos[0] < car.pos[0] and (car.pos[0] - self.pos[0]) <= 60):
                        min_val = self.find_smol_value(self.decelerate_on_x(), min_val)
                    if(self.pos[0] < car.pos[0] and (car.pos[0] - self.pos[0]) <= 30):
                        min_val = self.find_smol_value(0, min_val)
                if(car.pos[1] == self.pos[1] and self.direction == 1 and car.direction == 1):
                    if(self.pos[0] > car.pos[0] and (self.pos[0] - car.pos[0]) <= 60):
                        max_val = self.find_big_value(self.decelerate_on_x(), max_val)
                    if(self.pos[0] > car.pos[0] and (self.pos[0] - car.pos[0]) <= 30):
                        max_val = self.find_big_value(0, max_val)
                if(car.pos[0] == self.pos[0] and self.direction == 2 and car.direction == 2):
                    if(self.pos[1] > car.pos[1] and (self.pos[1] - car.pos[1]) <= 60):
                        max_val = self.find_big_value(self.decelerate_on_y(), max_val)
                    if(self.pos[1] > car.pos[1] and (self.pos[1] - car.pos[1]) <= 30):
                        max_val = self.find_big_value(0, max_val)
                if(car.pos[0] == self.pos[0] and self.direction == 3 and car.direction == 3):
                    if(self.pos[1] < car.pos[1] and (car.pos[1] - self.pos[1]) <= 60):
                        min_val = self.find_smol_value(self.decelerate_on_y(), min_val)
                    if(self.pos[1] < car.pos[1] and (car.pos[1] - self.pos[1]) <= 30):
                        min_val = self.find_smol_value(0, min_val)
        if(self.direction == 0):
            min_val = self.find_smol_value(self.accelerate_on_x(), min_val)
        if(self.direction == 1):
            max_val = self.find_big_value(self.accelerate_on_x(), max_val)
            return max_val
        if(self.direction == 2):
            max_val = self.find_big_value(self.accelerate_on_y(), max_val)
            return max_val
        if(self.direction == 3):
            min_val = self.find_smol_value(self.accelerate_on_y(), min_val)
        return min_val
                


    def accelerate_on_x(self):
        if(abs(self.speed[0]) + self.acceleration <= self.max_speed):
            if(self.direction == 0):
                return (self.speed[0] + self.acceleration)
            else:
                return (self.speed[0] - self.acceleration)
        return self.speed[0]

    def decelerate_on_x(self):
        if(self.direction == 0):
            if(self.speed[0] - (self.acceleration * 2) > 0):
                return (self.speed[0] - self.acceleration * 2)
        else:
            if(self.speed[0] + (self.acceleration * 2) < 0):
                return (self.speed[0] + self.acceleration * 2)
        return self.speed[0]
    
    def accelerate_on_y(self):
        if(abs(self.speed[1]) + self.acceleration <= self.max_speed):
            if(self.direction == 3):
                return (self.speed[1] + self.acceleration)
            else:
                return (self.speed[1] - self.acceleration)
        return self.speed[1]

    def decelerate_on_y(self):
        if(self.direction == 3):
            if(self.speed[1] - (self.acceleration * 2) > 0):
                return (self.speed[1] - self.acceleration * 2)
        else:
            if(self.speed[1] + (self.acceleration * 2) < 0):
                return (self.speed[1] + self.acceleration * 2)
        return self.speed[1]

class Trafficlight(Agent):
    def __init__(self, model, pos, dir):
        super().__init__(model.next_id(), model)
        self.pos = pos
        self.state = False
        self.direction = dir
        
    
    def step(self):
        if(self.model.schedule.steps % 100 == 0):
            self.state = not(self.state)


class City(Model):
    def __init__(self, C = 600, R = 600):
        super().__init__()
        self.schedule = RandomActivation(self)
        self.grid = MultiGrid(C, R, torus=False)

        matrix = np.zeros((C,R))
        routes = [[599,290]]
        routesVert = [[310,599]]
        routes2 = [[1, 315]]
        routesVert2 = [[290,1]]
        # Izquierda, Derecha, Arriba, Abajo
        originList = [[0,290], [20, 290], [599, 315], [580, 315], [290, 599], [290, 580], [310, 1], [310, 20]]
        routeL1 = [[65,290], [65,65], [290, 65], [290, 0]]
        routeL2 = [[290,290], [290,95], [90, 95], [90, 505], [290, 505], [290, 315], [1, 315]]
        routeR1 = [[310, 315], [310, 599]]
        routeR2 = [[535, 315], [535, 535], [310, 535], [310, 599]]
        routeU1 = [[290,1]]
        routeU2 = [[290,95], [90, 95], [90, 290], [599, 290]]
        routeD1 = [[310, 65], [535, 65], [535, 535], [65, 535], [65, 65], [290, 65], [290, 1]]
        routeD2 = [[310, 505], [508, 505], [508, 315], [90, 315], [90, 505], [290, 505], [290, 1]]
        routeL1C = [[65,290], [65,65], [290, 65], [290, 0]]
        routeL2C = [[290,290], [290,95], [90, 95], [90, 505], [290, 505], [290, 315], [1, 315]]
        routeR1C = [[310, 315], [310, 599]]
        routeR2C = [[535, 315], [535, 535], [310, 535], [310, 599]]
        routeU1C = [[290,1]]
        routeU2C = [[290,95], [90, 95], [90, 290], [599, 290]]
        routeD1C = [[310, 65], [535, 65], [535, 535], [65, 535], [65, 65], [290, 65], [290, 1]]
        routeD2C = [[310, 505], [508, 505], [508, 315], [90, 315], [90, 505], [290, 505], [290, 1]]
        routeList = [routeL1, routeL2, routeR1, routeR2, routeU1, routeU2, routeD1, routeD2]
        routeList2 = [routeL1C, routeL2C, routeR1C, routeR2C, routeU1C, routeU2C, routeD1C, routeD2C]
        trafficlights = [[255,290,0], [330,315,1], [290,350,2], [310,280,3], 
                        [48,290,0], [107,315,1], [65,334,2], [90,273,3], 
                        [491,290,0], [550,315,1], [508,334,2], [535,274,3],
                        [269,65,0], [328,95,1], [290,114,2], [310,50,3],
                        [270,505,0], [328,535,1], [290,552,2], [310,489,3]]
        carsList = []
        trafficList = []

        for i in range(8):
            carType = randint(1,5)
            randRoute = randint(0,1)
            if(i%2 == 0):
                car = Car(self, (originList[i][0], originList[i][1]), routeList[i + randRoute], [1, 0], 1, carType, carsList, trafficList)
                carsList.append(car)

                #Se posicionan los carros en el modelo
                self.grid.place_agent(car, car.pos)
                self.schedule.add(car)
            else:
                car2 = Car(self, (originList[i][0], originList[i][1]), routeList2[i - randRoute], [1, 0], 1, carType, carsList, trafficList)
                carsList.append(car2)

                #Se posicionan los carros en el modelo
                self.grid.place_agent(car2, car2.pos)
                self.schedule.add(car2)


        #Se construyen los semaforos
        for i in range(0, len(trafficlights)):
            trafficl = Trafficlight(self, (trafficlights[i][0], trafficlights[i][1]), trafficlights[i][2])
            self.grid.place_agent(trafficl, trafficl.pos)
            #Se definen los semaforos en luz verde
            if(trafficlights[i][2] == 0 or trafficlights[i][2] == 1):
                trafficl.state = True
            self.schedule.add(trafficl)
            trafficList.append(trafficl)
        

    def step(self):
            self.schedule.step()

"""def agent_portrayal(agent):
    if(type(agent) == Car):
        if(agent.unique_id == 1):
            return {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "Color": "#D0ACEF", "Layer": 1}
        elif (agent.unique_id == 2):
            return {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "Color": "Blue", "Layer": 1}
        else:
            return {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "Color": "Pink", "Layer": 1}
    if(type(agent) == Trafficlight):
        if(agent.state):
            return {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "Color": "Green", "Layer": 1}
        else:
            return {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "Color": "Red", "Layer": 1}
    else:
        return {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "Color": "Gray", "Layer": 0}

grid = CanvasGrid(agent_portrayal, 60, 60, 450, 450)
text = TotalSteps()
server = ModularServer(City, 
                        [grid, text], 
                        "Simulación trafico", {})
server.port = 8525
server.launch() """